const fs = require('fs');

function writeUserDataToFile(data){
  var jsonUserData = JSON.stringify(data);
  fs.writeFile("./usuarios.json", jsonUserData, "utf8",
    function(err){
      if(err){
        console.log("writeUserDataToFile.- ERROR: "+err);
      }else{
        console.log("writeUserDataToFile.- Array users escrito en fichero usuarios.json");
      }
    }
  );
}

module.exports.writeUserDataToFile = writeUserDataToFile;
