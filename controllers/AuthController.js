const io = require('../io');
const utils = require('../utils/Utils');


function loginV1(req, res){
    console.log("POST /apitechu/v1/login");
  
    var email = req.body.email;
    var pass = req.body.password;
    //TODO: Preguntar forma mas optima de ver que no es undefind, null ni cadena vacia
    if(email && pass){
      var users = require('../usuarios.json');
      //BUCLE FINDINDEX
      var userFound = utils.findIndexEmailPass(users, email,pass);

      console.log("userFound: "+userFound);
      if(userFound != -1){
        //USUARIO SI EXISTE.- PONEMOS PROPIEDAD logged:true
        users[userFound].logged=true;
        //Escribo en fichero
        io.writeUserDataToFile(users);

        res.send({"mensaje":"Login correcto", "idUsuario":users[userFound].id});


      }else{
        //USUARIO NO ENCONTRADO
       res.status(409).send({"mensaje":"Login incorrecto"});
      }

    }else{
      //Parametros incorrectos
      console.log("Parametros no informados.- Login incorrecto");
      //res.send({"mensaje":"Login incorrecto"});
      res.status(400).send({"mensaje":"Login incorrecto"});
    }

    console.log("FIN LOGIN");
}

function logoutV1(req, res){
  console.log("POST /apitechu/v1/logout");
  console.log("La ID del usuario a desconectar es:"+req.params.id);

  var users = require('../usuarios.json');
  //BUCLE FINDINDEX
  var userFound = utils.findIndexIdLogged(users, req.params.id);
  console.log("userFound: "+userFound);
  if(userFound != -1){
    //USUARIO SI EXISTE Y ESTA LOGADO.- QUITAMOS PROPIEDAD logged=true
    delete users[userFound].logged;
    //Escribo en fichero
    io.writeUserDataToFile(users);
    res.send({"mensaje":"Logout correcto", "idUsuario":req.params.id});
  }else{
    //USUARIO NO ENCONTRADO
   res.status(409).send({"mensaje":"Logout incorrecto"});
  }
  console.log("FIN LOGIN");
}



module.exports.loginV1 = loginV1;
module.exports.logoutV1 = logoutV1;
