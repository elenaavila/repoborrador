const io = require('../io');
const utils = require('../utils/Utils');
const crypt = require('../utils/Crypt');
const requestJson = require('request-json');

const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechueam12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function getUsersV1(req, res){
  console.log("GET /apitechu/v1/users");
  var users = require('../usuarios.json');
  var responseUsers = {};
  //res.sendFile("usuarios.json", {root: __dirname});
  if(req.query.$count && req.query.$count === "true"){
    //Añadir el campo count
      console.log("users.length - campo total:"+users.length);
      responseUsers.count = users.length;
  }

  if(req.query.$top){
    console.log("TOP:"+req.query.$top);
    responseUsers.users = users.slice(0, req.query.$top);
  }else{
    responseUsers.users = users;
  }

  //Enviamos la respuesta
  res.send(responseUsers);
}

function createUserV1(req, res){
    console.log("POST /apitechu/v1/users");
    console.log(req.body);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUser = {
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email
    }
    console.log(newUser);

    var users = require('../usuarios.json');
    users.push(newUser);
    console.log("Usuario añadido al array");

    io.writeUserDataToFile(users);

    res.send({"msg":"Usuario creado"});
}

function deleteUserV1(req, res){
  console.log("DELETE /apitechu/v1/users/:id");
  console.log("La ID del usuario a borrar es:"+req.params.id);
  var users = require('../usuarios.json');

  //BUCLE FOR NORMAL:
  var found = utils.bucleForNormal(users, req.params.id);

  //BUCLE FOR IN:
  var found = utils.bucleForIn(users, req.params.id);

  //BUCLE FOR EACH:
  var found = utils.bucleForEach(users, req.params.id);

  //BUCLE FOR OF:
  var found = utils.bucleForOf(users, req.params.id);

  //BUCLE FINDINDEX
  var found = utils.bucleFindIndex(users, req.params.id);

  //BORRADO USUARIO SI EXISTE
  console.log("found: "+found);
  if(found != -1){
     users.splice(found, 1);
     io.writeUserDataToFile(users);
     res.send({"msg":"Usuario "+req.params.id+ " borrado"});
  }else{
     res.send({"msg":"El usuario "+req.params.id+ " no existe en fichero"});
  }
}

//VERSION 2

function getUsersV2(req, res){
  console.log("GET /apitechu/v2/users");
  var httpClient = requestJson.createClient(baseMLABUrl);
  console.log("Client created");

  httpClient.get("user?"+mLabAPIKey,
    function(err, resMlab, body){
      var response = !err ? body : {"msg" : "Error obteniendo usuarios"}

      //Enviamos la respuesta
      res.send(response);
    }
  );
}

function getUserByIdV2(req, res){
  console.log("GET /apitechu/v2/users/:id");
  var userId = Number.parseInt(req.params.id);
  console.log("La ID del usuario a obtener es "+userId);

  var httpClient = requestJson.createClient(baseMLABUrl);
  var query = "q=" + JSON.stringify({"id": userId});
  console.log("QUERY GETUSERBYID: " + query);
  httpClient.get("user?" + query + "&" + mLabAPIKey,
    function(err, resMlab, body){
      if(err){
        var response = {"msg" : "Error obteniendo usuario"};
        res.status(500);
      }else{
        if(body.length > 0){
          var response = body[0];
        }else{
          var response = {"msg" : "Usuario no encontrado"};
          res.status(404);
        }
      }
      //Enviamos la respuesta
      res.send(response);
    }
  );
}

function createUserV2(req, res){
    console.log("POST /apitechu/v2/users");
    console.log(req.body.id);
    console.log(req.body.first_name);
    console.log(req.body.last_name);
    console.log(req.body.email);

    var newUser = {
      "id": req.body.id,
      "first_name" : req.body.first_name,
      "last_name" : req.body.last_name,
      "email" : req.body.email,
      "password" : crypt.hash(req.body.password)
    }

    var httpClient = requestJson.createClient(baseMLABUrl);
    httpClient.post("user?"+mLabAPIKey, newUser,
      function(err, resMlab, body){
        console.log("Usuario creado");
        if(err){
          var response = {"msg" : "Error creando usuario"};
          res.status(500);
        }else{
          var response = {"msg" : "Usuario creado"};
          res.status(201);
        }
        //Enviamos la respuesta
        res.send(response);
      }
    );

}

module.exports.getUsersV1 = getUsersV1;
module.exports.createUserV1 = createUserV1;
module.exports.deleteUserV1 = deleteUserV1;

module.exports.getUsersV2 = getUsersV2;
module.exports.getUserByIdV2 = getUserByIdV2;
module.exports.createUserV2 = createUserV2;
