const io = require('../io');
const utils = require('../utils/Utils');
const crypt = require('../utils/Crypt');
const requestJson = require('request-json');

const baseMLABUrl="https://api.mlab.com/api/1/databases/apitechueam12ed/collections/";
const mLabAPIKey = "apiKey=" + process.env.MLAB_API_KEY;

function loginV2(req, res){
    console.log("POST /apitechu/v2/login");

    var email = req.body.email;
    var pass = req.body.password;

    if(email && pass){
      console.log("La email del usuario es "+email);
      var httpClient = requestJson.createClient(baseMLABUrl);
      var query = "q=" + JSON.stringify({"email": email});
      console.log("QUERY EMAIL: user?" + query + "&" + mLabAPIKey);
      httpClient.get("user?" + query + "&" + mLabAPIKey,
        function(err, resMlab, body){
          console.log("EN FUNCTION GETUSER email:"+email);
          if(err){
            var response = {"msg" : "Error obteniendo usuario"};
            res.status(500);
            res.send(response);
          }else{
            if(body.length > 0){
              var userFound = body[0];
              var passOK = crypt.checkPassword(pass, userFound.password);
              if(passOK === true){
                var id = Number.parseInt(userFound.id);
                var queryId = "q=" + JSON.stringify({"id": id});
                console.log("QUERY ID: " + queryId);
                //*PUT PARA ACTUALIZAR EL USUARIO CON LOGGED TRUE*
                var queryPutBody = '{"$set":{"logged":true}}';
                httpClient.put("user?" + queryId + "&" + mLabAPIKey, JSON.parse(queryPutBody),
                  function(err, resMlab, body){
                    if(err){
                      var response = {"mensaje" : "Error actualizando usuario"};
                      res.status(500);
                    }else{
                      var response = {"mensaje":"Login correcto", "idUsuario":userFound.id};
                    }
                    //Enviamos la respuesta
                    res.send(response);
                  }
                );
              }else{
                //PASS INCORRECTA
                res.status(409).send({"mensaje":"Login incorrecto"});
              }
            }else{
              //Enviamos la respuesta
              res.status(404).send({"mensaje" : "Usuario no encontrado"});
            }
          }
        }
      );
    }else{
      //Parametros incorrectos
      console.log("Parametros no informados.- Login incorrecto");
      //res.send({"mensaje":"Login incorrecto"});
      res.status(400).send({"mensaje":"Login incorrecto"});
    }
    console.log("FIN LOGIN");
}

function logoutV2(req, res){
  console.log("POST /apitechu/v2/logout");
  console.log("La ID del usuario a desconectar es:"+req.params.id);
  var userId = Number.parseInt(req.params.id);
  var httpClient = requestJson.createClient(baseMLABUrl);

  var queryLogged = 'q='+JSON.stringify({ "$and":[{"id":userId}, {"logged":true}]});
  console.log("LOGOUT QUERY: user?" + queryLogged + "&" + mLabAPIKey);
  httpClient.get("user?" + queryLogged + "&" + mLabAPIKey,
    function(err, resMlab, body){
      console.log("EN FUNCTION GETUSER id:"+userId);
      if(err){
        res.status(500).send({"mensaje" : "Error obteniendo usuario"});
      }else{
        if(body.length > 0){
          var userFound = body[0];
          //USUARIO SI EXISTE Y ESTA LOGADO.- QUITAMOS PROPIEDAD logged=true
          //*PUT PARA ACTUALIZAR EL USUARIO CON LOGGED TRUE*
          var queryPutBody = '{"$unset":{"logged":""}}';
          httpClient.put("user?" + queryLogged + "&" + mLabAPIKey, JSON.parse(queryPutBody),
            function(err, resMlab, body){
              if(err){
                var response = {"mensaje" : "Error logout usuario"};
                res.status(500);
              }else{
                var response = {"mensaje":"Logout correcto", "idUsuario":userFound.id};
              }
              //Enviamos la respuesta
              res.send(response);
            }
          );

        }else{
          //Enviamos la respuesta
          res.status(409).send({"mensaje" : "Logout incorrecto"});
        }
      }
    }
  );
  console.log("FIN LOGOUT");
}



module.exports.loginV2 = loginV2;
module.exports.logoutV2 = logoutV2;
