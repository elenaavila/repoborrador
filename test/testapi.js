const mocha = require('mocha');
const chai = require('chai');
const chaihttp = require('chai-http');

chai.use(chaihttp);
//Carga las Aserciones de tipo should de la lib chai
var should = chai.should();


describe("First test",
  function(){
    //Cada it es un test unitario. describe puede tener mas de un it
    it('Tests that duckduckgo works', function (done){
        chai.request('http://www.duckduckgo.com')
          .get('/')
          .end(//Funcion manejadora
            function(err, res){
              //Aqui iran las aserciones
              console.log("Request finished");
              //console.log("err: "+err);
              //console.log("res: "+JSON.stringify(res));
              //Fin del test
              done();
            }
          )
      }
    )
  }
);

describe("Test de API de usuarios",
  function(){
    //Cada it es un test unitario. describe puede tener mas de un it
    it('Tests that user api says hello', function (done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/hello')
          .end(//Funcion manejadora
            function(err, res){
              //Aqui iran las aserciones
              console.log("Request finished");
              res.should.have.status(200);
              res.body.msg.should.be.eql("Hola desde API TechU");
              //Fin del test
              done();
            }
          )
      }
    ),
    it('Tests that user api returns user list', function (done){
        chai.request('http://localhost:3000')
          .get('/apitechu/v1/users')
          .end(//Funcion manejadora
            function(err, res){
              //Aqui iran las aserciones
              console.log("Request users finished");
              res.should.have.status(200);
              res.body.users.should.be.a('array');

              for(user of res.body.users){
                user.should.have.property('id');
                user.should.have.property('email');
                user.should.have.property('password');
              }
              //Fin del test
              done();
            }
          )
      }
    )

  }
);


//Documento MongoDB
{
  "first_name":"Server",
  "last_name":"iano",
  "quesos": ["Dehesa de los Llanos", "Montescusa"],
  "amigotes": [
    {
      "name":"Manolo",
      "de_quien_es": "el de Pistolas"
    },
    {
      "name":"Indalecio",
      "de_quien_es": "el de Pelala"
    }
  ],
  "queso_favorito":{
    "name": "Dehesa de los LLanos",
    "milk": "cabra"
  }
}
