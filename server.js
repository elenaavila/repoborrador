require('dotenv').config();
const express = require('express');
const app = express();
const io = require('./io');
const usersController = require('./controllers/UsersController');
const authController = require('./controllers/AuthController');
const authControllerV2 = require('./controllers/AuthControllerV2');

app.use(express.json());//Parsea los body de las peticiones a json

const port = process.env.PORT || 3000;
app.listen(port);

console.log("API escuchando en el puerto "+port);

app.get("/apitechu/v1/hello",
  function(req, res){
    console.log("GET /apitechu/v1/hello");

    res.send({"msg":"Hola desde API TechU"});
  }
)

app.post("/apitechu/v1/monstruo/:p1/:p2",
  function(req, res){
    console.log("POST /apitechu/v1/monstruo/:p1/:p2");
    console.log("Parámetros");
    console.log(req.params);

    console.log("Query String");
    console.log(req.query);

    console.log("Headers");
    console.log(req.headers);

    console.log("Body");
    console.log(req.body);
    console.log(req.body.body);
    console.log(req.body.body.msg);
  }
)

app.get("/apitechu/v1/users", usersController.getUsersV1);
app.post("/apitechu/v1/users", usersController.createUserV1);
app.delete("/apitechu/v1/users/:id", usersController.deleteUserV1);

app.post("/apitechu/v1/login", authController.loginV1);
app.post("/apitechu/v1/logout/:id", authController.logoutV1);

//Version 2
app.get("/apitechu/v2/users", usersController.getUsersV2);
app.get("/apitechu/v2/users/:id", usersController.getUserByIdV2);
app.post("/apitechu/v2/users", usersController.createUserV2);

app.post("/apitechu/v2/login", authControllerV2.loginV2);
app.post("/apitechu/v2/logout/:id", authControllerV2.logoutV2);
