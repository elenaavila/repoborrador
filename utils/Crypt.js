const bcrypt = require('bcrypt');

function hash(data){
  console.log("Hashing data");
  //Devuelve un String
  return bcrypt.hashSync(data, 10);
}

function checkPassword(passwordFromUserInPlainText, passwordFromDBHashed) {
  //console.log("Checking password:"+passwordFromUserInPlainText+"-passwordFromDBHashed:"+passwordFromDBHashed);
  // Returns boolean
  return bcrypt.compareSync(passwordFromUserInPlainText, passwordFromDBHashed);
}

module.exports.hash = hash;
module.exports.checkPassword = checkPassword;
