function bucleForNormal(users, elementId){
  console.log("bucleForNormal- Buscar elemento:"+elementId);
  for(var i=0; i<users.length;i++){
    if(users[i].id == elementId){
      console.log("bucleForNormal ENCONTRADO"+JSON.stringify(users[i]));
      console.log("bucleForNormal ENCONTRADO en posicion "+i);
      return i;
    }
  }
  return -1;
}

function bucleForIn(users, elementId){
  console.log("bucleForIn- Buscar elemento:"+elementId);
  for (numUser in users) {
    if(users[numUser].id == elementId){
      console.log("bucleForIn ENCONTRADO"+JSON.stringify(users[numUser]));
      console.log("bucleForIn ENCONTRADO en posicion "+numUser);
      return numUser;
    }
  }
  return -1;
}

function bucleForEach(users, elementId){
  console.log("bucleForEach- Buscar elemento:"+elementId);
  var numElement = -1;
  users.forEach(function(element, index, users) {
    console.log("bucleForEach element: "+JSON.stringify(element));
    if(element.id == elementId){
      console.log("bucleForEach ENCONTRADO"+JSON.stringify(element));
      console.log("bucleForEach ENCONTRADO en posicion "+index);
      numElement = index;
    }
  });
  return numElement;
}

function bucleForOf(users, elementId){
  var contador = 0;
  console.log("bucleForOf- Buscar elemento:"+elementId);
  for (user of users) {
    if(user.id == elementId){
      console.log("bucleForOf ENCONTRADO"+JSON.stringify(user));
      console.log("bucleForOf ENCONTRADO en posicion "+contador);
      return contador;
    }
    contador++;
  }

  /* DESTRUCTURING, nodeJS v6+
  for (var [index, user] of users.entries()) {
    if(user.id == elementId){
      console.log("bucleForOf ENCONTRADO"+JSON.stringify(user));
      console.log("bucleForOf ENCONTRADO en posicion "+index);
      return index;
    }
  }*/
  return -1;
}

function bucleFindIndex(users, elementId){
  console.log("bucleFindIndex- Buscar elemento:"+elementId);
  //Devuelve la posicion del elemento si lo encuentra. Si no , -1
  return users.findIndex(function(element) {
    console.log("bucleFindIndex USER:"+JSON.stringify(element));
    //Hasta que no sea true, el findIndex (bucle) no para, sigue iterando.
    return element.id == elementId;
  });
}

//BUSQUEDA USUARIOS POR EMAIL Y PASSWORD
function findIndexEmailPass(users, email, pass){
  console.log("findIndexEmailPass- Buscar usuario con mail:"+email+"-pass:"+pass);
  //Devuelve la posicion del elemento si lo encuentra. Si no , -1
  return users.findIndex(function(element) {
    console.log("findIndexEmailPass USER:"+JSON.stringify(element));
    //Hasta que no sea true (email y pass), el findIndex (bucle) no para, sigue iterando.
    return (element.email == email && element.password == pass);
  });
}
//BUSQUEDA USUARIOS POR ID Y LOGGED=TRUE
function findIndexIdLogged(users, id){
  console.log("findIndexIdLogged- Buscar usuario con id:"+id);
  //Devuelve la posicion del elemento si lo encuentra. Si no , -1
  return users.findIndex(function(element) {
    console.log("findIndexIdLogged USER:"+JSON.stringify(element));
    //Hasta que no sea true (id y logged true), el findIndex (bucle) no para, sigue iterando.
    return (element.id == id && element.logged === true);
  });
}

module.exports.bucleForNormal = bucleForNormal;
module.exports.bucleForIn = bucleForIn;
module.exports.bucleForEach = bucleForEach;
module.exports.bucleForOf = bucleForOf;
module.exports.bucleFindIndex = bucleFindIndex;
module.exports.findIndexEmailPass = findIndexEmailPass;
module.exports.findIndexIdLogged = findIndexIdLogged;
